#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

require 'fileutils'

Dir.chdir('src')

Dir.glob('*.*').each do |f|
  if File.extname(f) != '.rb'
    d = f.downcase
    File.rename(f, d)
    if File.extname(d) != '.jpg' && File.extname(d) != '.gif' && File.extname(d) != '.pdf'
      system("convert '#{d}' '#{File.basename(d, '.*')}.jpg'")
      FileUtils.rm(d)
    end
  end
end
